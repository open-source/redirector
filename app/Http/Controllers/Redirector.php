<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class Redirector extends Controller
{
    public static function parseKeyWord(Request $request, $keyword){
        $entry = DB::table('redirects')->where('keyword', $keyword)->first();
        if($entry == null)
            abort(404);
        $inputs = $request->all();
        $url = url($entry->url);

        $queryParameters = "";
        foreach($inputs as $key => $value){
            $queryParameters .= $key . "=" . $value . "&";
        }
        $queryParameters = rtrim($queryParameters, "&");
        if(strlen($queryParameters) > 0){
            # Append the query Parameters
            if(strpos($url, "?") !== FALSE && strpos($url, "?") != (strlen($url)-1)){
                $url .= "&" . $queryParameters;
            }else{
                if(strpos($url, "?") === FALSE){
                    $url .= "?";
                }
                $url .= $queryParameters;
            }
        }
        return redirect($url);
    }
}
