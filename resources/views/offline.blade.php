<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        @font-face { font-family: LiberationSans; src: url('/fonts/liberation-sans/LiberationSans-Regular.ttf')}
        @font-face { font-family: LiberationSansBI; src: url('/fonts/liberation-sans/LiberationSans-BoldItalic.ttf')}
        body {
            font-family: LiberationSans;
        }
        #logo {
            text-align: center;
        }
        #logo a {
            text-decoration: none;
            font-size: 64px;
            color: rgb(255,128,0);
            font-style: italic;
            font-weight: bold;
        }
        #container {
            max-width: 600px;
            margin: 0 auto;
            margin-top: 50px;
        }

        #info {
            text-align: justify;
            font-size: 20px;
        }

        #footer {
            display:flex;
            justify-content: space-around;
        }
        #footer a {
            text-decoration: none;
            color: rgb(255,128,0);
        }
        #footer a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div id="container">
        <div id="logo">
            <a href="https://metager.to">MetaGer.to</a>
        </div>
        <p id="info">
                Dieser Dienst wird aufgrund der Einführung der DSGVO nach-und-nach
                eingestellt.  Die bestehenden und konfigurierten URL-Umleitungen werden
                zunächst in statische Links umgewandelt; eine Neuanmeldung ist aber ab
                sofort nicht mehr möglich.  Eine Protokollierung der Klicks findet nicht
                statt; personenbeziehbare Daten werden nicht gespeichert.  Zu einem späteren
                Zeitpunkt wird dieser Dienst komplett eingestellt werden.  Ausnahmen werden
                nur Linkumleitungen des SUMA-EV selber sein.
                
        </p>
        <div id="footer">
            <div><a href="https://metager.de/kontakt">Kontakt</a></div>
            <div><a href="https://metager.de/impressum">Impressum</a></div>
            <div><a href="https://metager.de/datenschutz">Datenschutz</a></div>
        </div>
    </div>

</body>
</html>